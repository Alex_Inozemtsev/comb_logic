package `type`


sealed trait Type{
  override def toString: String = this match {
    case Arrow(t1: TyVar, t2) => t1.toString + " -> " + t2.toString
    case Arrow(t1, t2) => "(" + t1.toString + ")" + " -> " + t2.toString
    case TyVar(s) => s
  }
}
case class TyVar(a: String) extends Type
case class Arrow(a: Type, b: Type) extends Type
