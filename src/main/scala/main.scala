import expr.Expr
import typeInference.Infer
import scala.io.StdIn._
import scala.Console._
object main extends scala.App {

  val input =  readLine("Enter Combinator: ");
  val expr = Parser.ParseExpr(input) match {
    case Left(s) => println(s)
    case Right(expr) => println(Infer.inferenceTypes(expr).toString)
  }

  val comb = Expr.randExpr(2)
  println(comb.toString)
  println(Infer.inferenceTypes(comb).toString)
}
