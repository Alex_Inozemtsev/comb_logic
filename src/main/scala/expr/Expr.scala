package expr
import `type`.{Arrow, TyVar, Type}
import org.scalacheck.{Arbitrary, Gen}
import org.scalacheck.Gen._

class Expr{
  override def toString: String = this match {
    case App(expr1, expr2: C) => expr1.toString + expr2.toString
    case App(expr1, expr2) => expr1.toString + "(" + expr2.toString + ")"
    case C(t: Name) => t.toString
  }

}

object Expr {
  val genCombinator: Gen[C] = Gen.frequency(
    (2, C(K)),
    (1, C(S)),
    (2, C(I))
  )
  val genCombinatorNoI: Gen[C] = Gen.frequency(
    (5, C(K)),
    (3, C(S)),
  )

  def genExpr(depth: Int, forLeft: Boolean): Gen[Expr] =
    if (depth == 0 && forLeft) genCombinatorNoI
    else if (depth == 0) genCombinator
    else for {
      left <- oneOf(genExpr(depth - 1, forLeft = true), genCombinatorNoI)
      right <- oneOf(genExpr(depth -1,forLeft = false ), genCombinator)
    } yield App(left,right)

  def randExpr(maxDepth: Int) = genExpr(maxDepth,false).sample.get
}
case class App(expr1: Expr, expr2: Expr) extends Expr

//C - Combinator
case class C(T: Name) extends Expr

trait Name{
  def ToString: String = this match {
    case I => "I"
    case K => "K"
    case S => "S"
  }
}

case object I extends Name {
  def GetType(names: IndexedSeq[String]): (Type, IndexedSeq[String]) = {
    val a = names(0)
    (Arrow(TyVar(a), TyVar(a)), names.drop(1))
  }
}
case object K extends Name {
  def GetType(names: IndexedSeq[String]): (Type, IndexedSeq[String]) = {
    val a = names(0)
    val b = names(1)
    (Arrow(TyVar(a), Arrow(TyVar(b), TyVar(a))), names.drop(2))
  }
}
case object S extends Name {
  def GetType(names: IndexedSeq[String]): (Type, IndexedSeq[String]) = {
    val a = names(0)
    val b = names(1)
    val c = names(2)
    (Arrow(
      Arrow(TyVar(a), Arrow(TyVar(b), TyVar(c))),
      Arrow(Arrow(TyVar(a), TyVar(b)), Arrow(TyVar(a), TyVar(c)))),
      names.drop(3))
  }
}



